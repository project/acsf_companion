<?php

namespace Acsf;

use Composer\Composer;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UpdateOperation;
use Composer\Installer\PackageEvent;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Util\ProcessExecutor;

class AcsfCompanionPlugin implements PluginInterface, EventSubscriberInterface
{
  const ACSF_MODULE_NAME = "drupal/acsf";
  const DRUSH_PACKAGE_NAME = "drush/drush";

  /**
   * AcsfCompanionPlugin constructor.
   */
  public function __construct() {
    $this->runAcsfInit = false;
  }

  /**
   * @inheritdoc
   */
  public function activate(Composer $composer, IOInterface $io) {
    $this->composer = $composer;
    $this->io = $io;
  }

  /**
   * @inheritdoc
   */
  public function deactivate(Composer $composer, IOInterface $io)
  {
    // Not implemented.
  }

  /**
   * @inheritdoc
   */
  public function uninstall(Composer $composer, IOInterface $io)
  {
    // Not implemented.
  }

  /**
   * Composer callback. Specifies the events the plugin wants to subscribe for.
   *
   * @return array \Composer\Script\Event handler map.
   */
  public static function getSubscribedEvents() {
     return array(
       'post-install-cmd' => 'onPostInstallOrUpdate',
       'post-update-cmd' => 'onPostInstallOrUpdate',
       'post-package-install' => 'onPostPackageInstall',
       'post-package-update' => 'onPostPackageUpdate',
     );
  }

  /**
   * Composer event handler. Called after a package has been installed.
   *
   * @param \Composer\Installer\PackageEvent $event Composer event.
   */
  public function onPostPackageInstall(PackageEvent $event) {
    $op = $event->getOperation();
    if ($op instanceof InstallOperation &&
        $op->getPackage()->getName() == self::ACSF_MODULE_NAME) {
      // We are installing the module. Schedule acsf-init execution.
      $this->runAcsfInit = true;
    }
  }

  /**
   * Composer event handler. Called after a package has been updated.
   *
   * @param \Composer\Installer\PackageEvent $event Composer event.
   */
  public function onPostPackageUpdate(PackageEvent $event) {
    $op = $event->getOperation();
    if ($op instanceof UpdateOperation &&
        $op->getInitialPackage()->getName() == self::ACSF_MODULE_NAME) {
      // We are updating the module. Schedule acsf-init execution.
      $this->runAcsfInit = true;
    }
  }

  /**
   * Composer event handler. Called after the entire install or update operation has been completed.
   *
   * @param \Composer\Script\Event $event Composer event.
   */
  public function onPostInstallOrUpdate(Event $event) {
    if ($this->runAcsfInit){
      $this->runAcsfInit();
    }
  }

  private function runAcsfInit() {
    // To run acsf-init command we need 2 packages. Acsf itself and drush.
    $acsf = $this->getPackage(self::ACSF_MODULE_NAME);
    $drush = $this->getPackage(self::DRUSH_PACKAGE_NAME);

    $docroot = $this->getDrupalDocroot();
    if ($acsf === null || $drush === null || $docroot === '') {
      return;
    }

    $installationManager = $this->composer->getInstallationManager();
    $cmd = sprintf('%s --include=%s --root=%s acsf-init -y',
      escapeshellarg($this->composer->getConfig()->get('bin-dir') . '/drush'),
      escapeshellarg($this->getRoot() . '/' . $installationManager->getInstallPath($acsf) . '/acsf_init'),
      escapeshellarg($docroot)
    );

    $stderr = '';
    if ($this->exec($docroot, $cmd, $stderr) == 0) {
      $this->io->write('acsf-init completed successfully.');
    } else {
      $this->io->warning('acsf-init has failed. You will have to rerun it manually.');
      $this->io->warning('For more information see: https://docs.acquia.com/site-factory/workflow/deployments/acsf-init');
      $this->io->warning('acsf-init stderr was:');
      $this->io->warning($stderr);
      $this->io->warning('The command attempted was:');
      $this->io->warning($cmd);
    }
  }

  private function exec(string $cwd, string $command, string &$stderr) {
    $pe = new ProcessExecutor($this->io);
    $out = '';
    $result = $pe->execute($command, $out, $cwd);
    if ($result != 0) {
      $stderr = $pe->getErrorOutput();
    }
    return $result;
  }

  private function getRoot(): string {
    return dirname($this->composer->getConfig()->get('vendor-dir'));
  }

  private function getDrupalDocroot() {
    $docroot = null;
    $root = $this->getRoot();
    // Assume it is a valid Drupal docroot if index.php is present.
    if (file_exists($root . '/docroot/index.php')) {
      $docroot = $root . '/docroot';
    } elseif (file_exists($root . '/index.php')) {
      $docroot = $root;
    }
    if (!$docroot) {
      $this->io->warning(sprintf('Could not find Drupal docroot in %s. acsf-init will not be run.', $root));
      return '';
    }
    return $docroot;
  }

  private function getPackage(string $name) {
    $pkg = $this->getLocalPackageByName($name);
    if ($pkg === null) {
      $this->io->warning(sprintf('%s package not found. acsf-init will not be run.', $name));
      return null;
    }
    return $pkg;
  }

  private function getLocalPackageByName(string $name) {
    $repositoryManager = $this->composer->getRepositoryManager();
    foreach ($repositoryManager->getLocalRepository()->getPackages() as $package) {
      if ($package->getName() === $name) {
        return $package;
      }
    }
    return null;
  }

  /**
   * The Composer;
   *
   * @var \Composer\Composer
   */
  private $composer;

  /**
   * Input/Output. Used to print to the console etc.
   *
   * @var \Composer\IO\IOInterface
   */
  private $io;

  /**
   * Flag indicating that acsf-init should be run by post-install/update handler.
   *
   * @var bool
   */
  private $runAcsfInit;
}
